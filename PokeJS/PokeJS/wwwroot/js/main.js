﻿var loc = document.getElementById('locationContainer');
var battle = document.getElementById('battleContainer');

window.onload = function () {
    loc.style.display = 'block';
    battle.style.display = 'none';
    render('/wwwroot/data/maps/test_map.json');
}

function changeLocation() {
    if (location.style.display != 'none') {
        loc.style.display = 'none';
        battle.style.display = 'block';
    }
    else {
        loc.style.display = 'block';
        battle.style.display = 'none';
    }
}