﻿var tileSize = 64;      //При изменении нужно изменить и css элемент tile

function render(file) {
    var map = getJson(file);
    for (var i = 0; i < map['height']; i++) {
        for (var g = 0; g < map['width']; g++) {
            var tile = map['tiles'][map['map'][i][g]];
            var img = document.createElement('img');
            img.src = '/wwwroot/data/images/' +  tile['img'];

            img.classList.add('tile');
            if (tile['wall']) img.classList.add('wall');

            img.style.left = (tileSize * g) + 'px';
            img.style.top = (tileSize * i) + 'px';
            var cont = document.getElementById('relL');
            cont.appendChild(img);
        }
    }
}

function getJson(file) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', file, false);
    xhr.send();
    if (xhr.status != 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    } else {
        return JSON.parse(xhr.responseText, true);
    }
}